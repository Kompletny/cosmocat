﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_Controller : MonoBehaviour
{
    // --- Variables ---  // 

    private float movementInputDirection;

    private int amoutOFJumpsLeft; 

    private Rigidbody2D rb;
    private Animator anim;

    public float movementSpeed = 10.00f;
    public float JumpForce = 10.00f;
    public float GroundCheckRadius;
    public float wallCheckDistance;

    public int amountOfJumps = 1;

    private bool isFacingRight = true;
    private bool isWalking = true;
    private bool isGround;
    private bool isTouchingWall;
    private bool canJump;
    private bool ifWallSliding;


    public Transform GroundCheck;
    public Transform wallCheck;

    public LayerMask whatIsGround;


     // ----------------  // 


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        amoutOFJumpsLeft = amountOfJumps;
    }

    
    void Update()
    {
        CheckInput();  
        CheckMovementDirection();    
        UpdateAnimations();  
        CheckIfCanJump();
        CheckIfwallSliding();
    }

    private void CheckIfwallSliding() 
    {
             
    }

    private void FixedUpdate() 
    {
        ApplyMovement();
        CheckSurroundings();
    }

    // ----------------  // 

    private void CheckSurroundings()
    {
        isGround = Physics2D.OverlapCircle(GroundCheck.position, GroundCheckRadius, whatIsGround);

        isTouchingWall = Physics2D.Raycast(wallCheck.position, transform.right, wallCheckDistance, whatIsGround);
    }


    private void CheckInput()
    {
        movementInputDirection = Input.GetAxisRaw("Horizontal");
        
        if (Input.GetButtonDown("Jump"))
        {
            Jump();
        }

    }

    private void Jump()
    {
        if (canJump)
        {
            rb.velocity = new Vector2(rb.velocity.x, JumpForce);
            amoutOFJumpsLeft--;
        }
    }

    private void CheckIfCanJump()
    {
        if(isGround && rb.velocity.y <= 0)
        {
            amoutOFJumpsLeft = amountOfJumps;
        }

        if(amoutOFJumpsLeft <= 0)
        {
            canJump = false;
        }
        else 
        {
            canJump = true;
        }
    }

    private void CheckMovementDirection()
    {
        if (isFacingRight && movementInputDirection < 0)
        {
            Flip();
        }
        else if (!isFacingRight && movementInputDirection > 0)
        {
            Flip();
        }

        if(rb.velocity.x != 0)
        {
            isWalking = true;
        }  
        else
        {
            isWalking = false;
        }
    }

    private void UpdateAnimations()
    {
        anim.SetBool("isWalking", isWalking);
        anim.SetBool("isGrounded", isGround);
        anim.SetFloat("yVelocity", rb.velocity.y);
    }

    private void ApplyMovement()
    {
        rb.velocity = new Vector2(movementSpeed * movementInputDirection, rb.velocity.y);
    }

    private void Flip()
    {
        isFacingRight = !isFacingRight;
        transform.Rotate(0.0f, 180.0f, 0.0f);
    }

    private void OnDrawGizmos() 
  {
      Gizmos.DrawWireSphere(GroundCheck.position, GroundCheckRadius);

      Gizmos.DrawLine(wallCheck.position, new Vector3(wallCheck.position.x + wallCheckDistance, wallCheck.position.y, wallCheck.position.z));
  }
}
